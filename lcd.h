///////////////////////////////////////////////////
// activy3xx LCD plugin for VDR
//
// Original written by:
//
// Meinrad Sauter <meinrad.sauter@gmx.de>
// andreas 'randy' weinberger <vdr@smue.org>
//
// rearranged, beautified and rewritten by:
//
// Markus Geisler <geisler@primusnetz.de>
//
// optimized by:
// Sascha Quint <sascha.quint@gmx.net>
//
// rearranged, beautified and rewritten by:
// Helmut Auer <helmut@helmutauer.de>
//
///////////////////////////////////////////////////

#ifndef __ALCD_LCD_H
#define __ALCD_LCD_H


#include <sys/time.h>
#include <sys/sem.h>
#include <vdr/tools.h>

// DEFINITIONS

#define LCD_PORT "/dev/ttyS0"

#define LCD_COLUMNS 20
#define LCD_ROWS     2

#define LEDREC_RED   0
#define LEDREC_GREEN 1

#define SCROLL_BLOCK 0
#define SCROLL_CHAR  1

#define OFF                  0
#define DARKEST              1
#define BRIGHTEST           16

#define PWRLED_RED           0
#define PWRLED_GREEN         1
#define PWRLED_BLINK         2

#define ALIGN_CENTER 0
#define ALIGN_LEFT   1 
#define ALIGN_RIGHT  2

#define LED_OFF 0
#define LED_ON  1

#define POWERLED_BLINK_ON        ((const BYTE *)"\x90\x80")
#define POWERLED_BLINK_OFF       ((const BYTE *)"\x90\x01")
#define POWERLED_GREEN           ((const BYTE *)"\x90\x08") // or "0x90 0x01"
#define POWERLED_RED             ((const BYTE *)"\x90\x10")
#define BASICLED_ON              ((const BYTE *)"\x90\x40")
#define BASICLED_OFF             ((const BYTE *)"\x90\x04")
#define MESSAGELED_ON            ((const BYTE *)"\x90\x20")
#define MESSAGELED_OFF           ((const BYTE *)"\x90\x02")

#define POWERBUTTON_ON           ((const BYTE *)"\x94\x12") // setkeycodes e010 xx
#define POWERBUTTON_OFF          ((const BYTE *)"\x94\x21")
#define POWERBUTTON_SCANCODE_ON  ((const BYTE *)"\x94\x03")
#define POWERBUTTON_SCANCODE_OFF ((const BYTE *)"\x94\x30")

#define MAGIC_RESET              ((const BYTE *)"\x95\x03")

#define LCD_WRITE_LINE0          ((const BYTE *)"\x9A\x02") // + \x00
#define LCD_WRITE_LINE1          ((const BYTE *)"\x9A\x03") // + \x00
#define LCD_CLEAR                ((const BYTE *)"\x9B\x00")

#define READBUTTONS              ((const BYTE *)"\x92\x00")

#define EVENTFILTER              ((const BYTE *)"\x95\xF0")
#define EVENTFILTEROFF           ((const BYTE *)"\x95\xFF")
#define BLAST                    ((const BYTE *)"\x85\x00")

#define HEARTBEAT                0x81

#define PICRESET                 ((const BYTE *)"\x8E\x00")
#define HWREV                    ((const BYTE *)"\x89\x00")
#define FWREV                    ((const BYTE *)"\x8A\x00")

#define LCD_PERM_ON              ((const BYTE *)"\x9c\x00")
#define LCD_PERM_OFF             ((const BYTE *)"\x9c\x01")

#define PIC_RES_ACK              0x80

#define ACTIVY_300               0
#define ACTIVY_3XX               1

#define EMPTY_LINE               (const BYTE *)"                    "

///////////////////////////////////////////////////
// class activyLCD

class activyLCD {
private:

   // filedescriptor for serial port
   int fd;
   BYTE actLine[LCD_ROWS][256];   
   unsigned int actPos[LCD_ROWS];
   int actBrightness;
   unsigned int scroll[LCD_ROWS];
   int lRecStat, lInfoStat, lPowerLEDStat;
   bool picError;
   bool bPermanent;
   bool bLocked;
   int fwRev;
   iconv_t cd;
   char *cdResult;
   size_t cdLength;


   int SerialPortOpen(void);
   void SerialPortClose(void);
   void PicWrite(const BYTE *command,int length);
   void Reset(void);
   bool bIsPlugin;
   void SetConv(void);
   void CloseConv(void);
   void Convert(const char *From, char *To, size_t ToLength);
public:

   // public member functions
   activyLCD(bool bPlugin = true);
   ~activyLCD();
   void SetLCDBrightness(const int brightness);
   void SetPowerButton(bool status);
   void SendCommand(const BYTE *command);
   void SendText(const BYTE *text[LCD_ROWS]);
   void SetHeartbeat(unsigned int timeVal = 0);
   void SetTextPermanent(bool bStatus);
   void SetInfo(int lRec, int lInfo, int lPwr );
   void FormatString( BYTE *output, const char *input, int len, bool bSplit = false, bool bAlign = false );
   void Lock(bool bStatus) {
      bLocked = bStatus;
   } 
   bool CheckErrors(void);
};

#endif
