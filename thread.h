///////////////////////////////////////////////////
// activy3xx LCD plugin for VDR
//
// Original written by:
//
// Meinrad Sauter <meinrad.sauter@gmx.de>
// andreas 'randy' weinberger <vdr@smue.org>
//
// rearranged, beautified and rewritten by:
//
// Markus Geisler <geisler@primusnetz.de>
//
// optimized by:
// Sascha Quint <sascha.quint@gmx.net>
//
// rearranged, beautified and rewritten by:
// Helmut Auer <helmut@helmutauer.de>
//
///////////////////////////////////////////////////

#ifndef __ALCD_THREAD_H
#define __ALCD_THREAD_H

#include <string>
#include <vector>
#include "lcd.h"
#include "setup.h"
#include <vdr/channels.h>
#include <vdr/thread.h>
#include <vdr/status.h>
using std::string;
using std::vector;

#define BUFSIZE 256

// aLCD wrapper thread class
class cLCD : public cThread, cStatus {
private:
   activyLCD *myLCD;

   struct RecordingInfo {
      string name;
      const cDevice *device;
   };
   typedef vector <RecordingInfo> Recordings;

   BYTE szChannelName[ BUFSIZE ];
   BYTE szProgName[ BUFSIZE ];
   BYTE szReplayName[ BUFSIZE ];
   BYTE szOsdTitle[ BUFSIZE ];
   BYTE szOsdText[ BUFSIZE ];
   BYTE szMsgText1[ BUFSIZE ];
   BYTE szMsgText2[ BUFSIZE ];
   BYTE szReplayMode[ 32 ];
   BYTE szVolText[ 21 ];

   int currentChannel;
   bool stop;
   bool running;
   int lLED1, lLED2, lLEDPower;
   int mLastVolume;
   int updCounter;

   int progDuration;
   int progStart;

   Recordings _recordings;
   cControl *PlayControl;

   int displayMode;
   int savedMode;
   int infoTimer;
   int lockTimer;
   cMutex _mutex;
   cCondVar _doUpdate;

   bool StoppedTimer(const char *szName);
   void SetInfoTimer();
   void GetProgram();

protected:
   virtual void Action(void);
   virtual void ChannelSwitch(const cDevice *Device, int ChannelNumber);
   virtual void Recording(const cDevice *Device, const char *Name, const char *FileName, bool On);
   virtual void Replaying(const cControl *Control, const char *Name, const char *FileName, bool On);
   virtual void SetVolume(int Volume, bool Absolute);
   virtual void OsdProgramme(time_t PresentTime, const char *PresentTitle, const char *PresentSubtitle, time_t FollowingTime, const char *FollowingTitle, const char *FollowingSubtitle);
   virtual void OsdCurrentItem(const char *Text);
   virtual void OsdChannel(const char *Text);
   virtual void OsdClear(void);
   virtual void OsdStatusMessage(const char *Message);
   virtual void OsdTitle(const char *Title);

public:
   void Lock(bool bStatus);
   bool IsLocked(void) {
      return lockTimer == 0 ? false : true;
   }
   void SetText(const char *Text);
   void SetLED(bool bLED1, bool bLED2);
   void SetPowerLED(int lPwrLED);
   void SetTextPermanent(bool bStatus);
   cLCD();
   ~cLCD();
};

#endif
