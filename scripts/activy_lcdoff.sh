#!/bin/sh
# Activy set Display text
source /etc/vdr.d/conf/vdr

[ "$(echo " $PLUGINS " | grep " alcd ")" = "" ] && exit
WAKEUP_FILE=/_config/status/Wakeup
WT=""
LCD_BR=$(grep "alcd.LCDBrightnessOnExit" /etc/vdr/setup.conf | cut -f 2 -d "=")
[ "$LCD_BR" = "" ] && LCD_BR=1
if [ -s $WAKEUP_FILE -a $LCD_BR -gt 0 ] ; then
   BREAK_MODE="1"
   NT=$(cat $WAKEUP_FILE | cut -f 1 -d ";")
   WT="$(date -d "1970-01-01 UTC $NT seconds" '+%d.%m.%Y - %R')"
   CH=$(cat $WAKEUP_FILE | cut -f 2 -d ";" |cut -b -2)
   PR=$(cat $WAKEUP_FILE | cut -f 3- -d ";" | cut -b -15)
   TEXT="$WT|$CH-$PR"
else
   LCD_BR="0"
   BREAK_MODE="0"
   TEXT=""
fi
afp-tool --powerled-blink=0 --basic-led=0 --message-led=0 --powerbutton=1 --brightness=$LCD_BR --stay=$BREAK_MODE --align=0 --text=$TEXT

