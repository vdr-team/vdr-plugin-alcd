///////////////////////////////////////////////////
// activy3xx LCD plugin for VDR
//
// Original written by:
//
// Meinrad Sauter <meinrad.sauter@gmx.de>
// andreas 'randy' weinberger <vdr@smue.org>
//
// rearranged, beautified and rewritten by:
//
// Markus Geisler <geisler@primusnetz.de>
//
// optimized by:
// Sascha Quint <sascha.quint@gmx.net>
//
// rearranged, beautified and rewritten by:
// Helmut Auer <helmut@helmutauer.de>
//
/////////////////////////////////////////////////////////

#ifndef __ACTIVYLCD_CPP
#define __ACTIVYLCD_CPP

#include<sys/wait.h>
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#include <vdr/tools.h>
#include <langinfo.h>
#include <locale.h>

#include "setup.h"
#include "lcd.h"

const BYTE *lcdBrightness[BRIGHTEST+1] = { (const BYTE *)"\xF0\xFF", (const BYTE *)"\xF0\xF0", 
   (const BYTE *)"\xF0\xE0", (const BYTE *)"\xF0\xD0", (const BYTE *)"\xF0\xC0", 
   (const BYTE *)"\xF0\xB0", (const BYTE *)"\xF0\xA0", (const BYTE *)"\xF0\x90", 
   (const BYTE *)"\xF0\x80", (const BYTE *)"\xF0\x70", (const BYTE *)"\xF0\x60", 
   (const BYTE *)"\xF0\x50", (const BYTE *)"\xF0\x40", (const BYTE *)"\xF0\x30", 
   (const BYTE *)"\xF0\x20", (const BYTE *)"\xF0\x10", (const BYTE *)"\xF0\x00"};

//
//// SQ: fuer Semaphor
#define KEY 4711 // Einduetigen Key

extern char *resetScript;

#undef D_EVENTFILTER

// Konstruktor
activyLCD::activyLCD(bool bPlugin) {
   BYTE * szLine[LCD_ROWS] = { (BYTE *)" ", (BYTE *)" "};

#ifdef ALCD_DEBUG
   isyslog("activyLCD");
#endif
   bIsPlugin = bPlugin;
   SetConv();

   actBrightness = -1;
   lRecStat = lInfoStat = lPowerLEDStat = -1;
   bLocked = false;
   bPermanent = false;

   if( resetScript != NULL )
      system( resetScript );

   fwRev = ACTIVY_300;

   SerialPortOpen();

   if( bIsPlugin ) {
      SendCommand(HWREV);
      SendCommand(FWREV);
      SetPowerButton(1);  // always enable PowerButton
      SetLCDBrightness(AlcdSetup.LCDBrightness);
      SetHeartbeat(0);

      SetInfo(LED_OFF,LED_OFF,PWRLED_GREEN);
      SendCommand(LCD_CLEAR);
      picError = false;
      SendText( (const BYTE **)szLine ); // for checking if Display is available
      if( picError ) {
         esyslog("activyLCD: Set Text failed - assuming no LCD available");
         //AlcdSetup.LCDavailable = false;
         picError = false;
      }
   }
   else
      actBrightness = AlcdSetup.LCDBrightness;

   actLine[0][0] = actLine[1][0] = '\x00';
   actPos[0] = actPos[1] = 0;
#ifdef ALCD_DEBUG
   isyslog("activyLCD: Constructor done");
#endif
}

/////////////////////////////////////////////////////////
// Destruktor

activyLCD::~activyLCD() {
#ifdef ALCD_DEBUG
   isyslog("~activyLCD");
#endif
   if( bIsPlugin ) {
      if( !bLocked )
         SendCommand(LCD_CLEAR);
      SetPowerButton(1);
      SetLCDBrightness(AlcdSetup.LCDBrightnessOnExit);
      if( bPermanent )
         SendCommand(LCD_PERM_ON);
      else
         SendCommand(LCD_PERM_OFF);
      SetInfo(LED_OFF,LED_OFF,-1);
      SetHeartbeat(0);
      SerialPortClose();
   }

   CloseConv();
#ifdef ALCD_DEBUG
   isyslog("acticyLCD: Destructor done");
#endif
}

/////////////////////////////////////////////////////////
// Open Serial Port

int activyLCD::SerialPortOpen(void) {
   struct termios options;

   // open serial device
   fd = open(LCD_PORT, O_RDWR | O_NOCTTY | O_NDELAY);

   // check result
   if( fd == -1 ) {
      // error - port could not be opened
      esyslog("alcd-plugin: SerPort could not be opened");
      return(-1);
   }
   else {
      // ok - port opened
      fcntl(fd, F_SETFL, 0);
      // Get the current options for the port...
      tcgetattr(fd, &options);
      // Set the baud rates to 38400...
      cfsetispeed(&options, B38400);
      cfsetospeed(&options, B38400);
      // Enable the receiver and set local mode...
      options.c_cflag |= (CLOCAL);
      options.c_cflag &= ~PARENB;
      options.c_cflag &= ~CSTOPB;
      options.c_cflag &= ~CSIZE;
      options.c_cflag |= CS8;
      options.c_oflag &= ~OPOST;
      options.c_oflag &= ~(ICANON | ECHO | ECHOE | ISIG);
      options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
      options.c_cc[VMIN]  = 0;                  /* warten auf min. 0 Zeichen */
      options.c_cc[VTIME] = 10;                 /* Timeout in deciseconds */

      // Set the new options for the port...
      tcsetattr(fd, TCSANOW, &options);
      // flush ser port
      tcflush(fd, TCIOFLUSH);

#ifdef ALCD_DEBUG
      isyslog("alcd-plugin: SerPort opened");
#endif
      return(fd);
   }
}

/////////////////////////////////////////////////////////
// Close Serial Port

void activyLCD::SerialPortClose() {
   close(fd);

#ifdef ALCD_DEBUG
   isyslog("alcd-plugin: SerPort closed");
#endif
}

/////////////////////////////////////////////////////////
// Send Command
void activyLCD::SendCommand(const BYTE *command) {
   if( !AlcdSetup.LCDavailable &&
       ( !memcmp( command, MESSAGELED_ON, 2 )   ||
         !memcmp( command, MESSAGELED_OFF, 2 )  ||
         !memcmp( command, LCD_WRITE_LINE0, 1 ) ||
         !memcmp( command, LCD_CLEAR, 2 )       ||
         !memcmp( command, lcdBrightness[0], 1 ) ) ) {
      return;
   }

   PicWrite( command, 2 );
}

/////////////////////////////////////////////////////////
// Send Command
void activyLCD::SendText(const BYTE *text[LCD_ROWS]) {
   BYTE buffer[ LCD_COLUMNS + 3 ];
   bool write = false;
   const BYTE *txt;

#ifdef ALCD_DEBUG
   isyslog( "SendText <%s><%s>", text[0], text[1] );
   char tmpBuf[128];
   for( int i = 0; i < 20 && text[0][i]; i++ ) {
      sprintf(tmpBuf + (2*i), "%02x", text[0][i] );
   }
   tmpBuf[40] = '-';
   for( int i = 0; i < 20 && text[1][i]; i++ ) {
      sprintf(tmpBuf + 41 + (2*i), "%02x", text[1][i] );
   }
   isyslog( "<%s>", tmpBuf );
#endif
   if( !AlcdSetup.LCDavailable )
      return;

   memset( buffer, 0, sizeof(buffer));
   for( int i=0; i < LCD_ROWS; i++ ) {
      if( text[i] == NULL )
         txt = EMPTY_LINE;
      else
         txt = text[i];


      if( strcmp( (char *)actLine[i], (char *)txt ) ) {
         memcpy( actLine[i], txt, strlen((char *)txt) + 1 );
         scroll[i] = actPos[i] = 0;
         write = true;
      }
      else {
         if( strlen( (char *)actLine[i] ) > LCD_COLUMNS && AlcdSetup.ScrollSpeed > 0 ) {
            if( ++(scroll[i]) > (unsigned int)AlcdSetup.ScrollSpeed ) {
               scroll[i] = 0;
               write = true;
               if( actPos[i] + LCD_COLUMNS >= strlen( (char *)actLine[i] ) )
                  actPos[i] = 0;
               else {
                  if( AlcdSetup.Scrolling == SCROLL_BLOCK )
                     actPos[i] += LCD_COLUMNS;
                  else
                     actPos[i]++;
               }
            }
         }
      }
#ifdef ALCD_DEBUG
      isyslog( "SendText <%s>", text[i] );
      isyslog( "ActLine <%s>", actLine[i] );
      isyslog( "ActPos <%d>", actPos[i] );
      isyslog( "Scroll <%d>", scroll[i] );
#endif
      if( write ) {
         //snesyslog( buffer, sizeof(buffer), "%s%-20.20s%c", i==0 ? LCD_WRITE_LINE0 : LCD_WRITE_LINE1, actLine[i] + actPos[i], '\x00');
         memcpy( buffer, i==0 ? LCD_WRITE_LINE0 : LCD_WRITE_LINE1, 2);
         int p = strlen((char *)actLine[i] + actPos[i]) < LCD_COLUMNS ? strlen((char *)actLine[i] + actPos[i]) : LCD_COLUMNS;
         memcpy( buffer + 2, actLine[i] + actPos[i], p);
         if( p < LCD_COLUMNS )
            memset( buffer + 2 + p, ' ', LCD_COLUMNS - p );

         buffer[ 2 + LCD_COLUMNS ] = '\x00';
#ifdef ALCD_DEBUG
         char tmpBuf[128];
         for( int j = 0; j < LCD_COLUMNS + 3; j++ ) {
            sprintf(tmpBuf + (2*j), "%02X", buffer[j] );
         }
         isyslog( "SendBuffer <%s>", tmpBuf );
#endif
         PicWrite( buffer, LCD_COLUMNS + 3 );
      }
   }
   SetLCDBrightness(AlcdSetup.LCDBrightness);
}


void activyLCD::SetTextPermanent(bool bStatus) {
   bPermanent = bStatus; 

   if( bPermanent )
      SendCommand(LCD_PERM_ON);
   else
      SendCommand(LCD_PERM_OFF);
}



/////////////////////////////////////////////////////////
// Send Command to LCD-Device
void activyLCD::PicWrite(const BYTE *command,int length) {
   BYTE picRes[6];
   int result;

   if( !command || !length )
      return;

#ifdef D_EVENTFILTER
   write(fd,EVENTFILTER,2);
   usleep(10000);
   read(fd,picRes, 2);

   // eventfilter set?
   CheckPicRes(picRes);
#endif

#ifdef ALCD_DEBUG
   isyslog("alcd-plugin: Send to lcd: <%02x%02x>", command[0], command[1]);
   if( (unsigned)command[0] == (unsigned)LCD_WRITE_LINE0[0] )
      isyslog("alcd-plugin: Text <%s> - %d",(char *)(command + 2), length);
#endif
#ifdef ALCD_DEBUG
   char tmpBuf[128];
   for( int i = 0; i < length; i++ ) {
      sprintf(tmpBuf + (2*i), "%02X", command[i] );
   }
   isyslog( "PicWrite <%s>", tmpBuf );
#endif

   // send real command to lcd
   write(fd, command,length);
   usleep(10000);
   result = read(fd,picRes,2);

   while( (result == 2) && (picRes[0] != command[0]) ) {
      isyslog("alcd-plugin: received: %d <%02x%02x>", result, picRes[0], picRes[1]);
      if( picRes[1] == command[0] ) {
         picRes[0] = command[0];
         result = read(fd,picRes+1,1) + 1;
      }
      else
         result = read(fd,picRes,2);
   }
#ifdef ALCD_DEBUG
   isyslog("alcd-plugin: result: %02x%02x (%d)", picRes[0], picRes[1], result);
#endif
   if( (result == 2) && (picRes[0] == command[0]) && (picRes[1] & PIC_RES_ACK) ) {
      // ergebniswerte auswerten
      switch( command[0] ) {
         // hw revision
         case 0x89 :
            // hw revision - 1 byte!
            read(fd,picRes+2,1);
            isyslog("alcd-plugin: Activy300-LCD-HW Revision: %i",picRes[2]);

            break;
            // fw revision
         case 0x8a :
            // fw revision - 4 bytes!
            read(fd,picRes+2,4);
            isyslog("alcd-plugin: Activy300-LCD-SW Revision: Release: %i Version: %i PreRelease: %c VirtualNumber: %i",
                   picRes[2],picRes[3],picRes[4],picRes[5] );

            if( picRes[2] == 2 && picRes[3] == 1 )
               fwRev = ACTIVY_3XX;
            else
               fwRev = ACTIVY_300;

            break;
            // blast ir buffer
         case 0x81 :
            // set LED
         case 0x85 :
            // send heartbeat
         case 0x90 :
            // set powerbutton
         case 0x94 :
            // clear lcd
         case 0x9b :
            // writeline
         case 0x9a :
            // set brightness
         case 0xf0 :
            // set brightness
            break;
            // buttons
         case 0x92 :
            // read buttons

            // bit 0 reserved
            // bit 1 power button
            // bit 2 up button
            // bit 3 down button
            // bit 4 left button
            // bit 5 right button
            // bit 6 select button
            // bit 7 reserved


            read(fd,picRes+2,1);
            picRes[2] &= ~0x81;
            if( picRes[2] != 0x00 )
               esyslog("alcd-plugin: %02x button pressed", picRes[2]);

            break;
         default :
            // command unknown!
            esyslog("alcd-plugin: ERROR: unknown command! <%02x%02x>", command[0], command[1]);
            break;
      }
   }
   else {
      picError = true;
      esyslog("alcd-plugin: Failing Command: <%02x%02x>", command[0], command[1]);
      esyslog("alcd-plugin: ResCode: %d <%02x%02x>",result, picRes[0], picRes[1]);
      if( command[0] == LCD_WRITE_LINE0[0] )
         esyslog("Failing Text <%s> - %d",(char *)command + 2, length);
   }

#ifdef D_EVENTFILTER
   write(fd,EVENTFILTEROFF,2);
   usleep(10000);
   read(fd,picRes, 2);

   // eventfilter set?
   CheckPicRes(picRes);
#endif

#ifdef ALCD_DEBUG
   isyslog("alcd-plugin: SendCommand done");
#endif
}

/////////////////////////////////////////////////////////
// Check Errors
bool activyLCD::CheckErrors() {
   bool bRc = false;

   if( picError == true ) {
      Reset();
      bRc = true;
   }
   return bRc;
}


void activyLCD::Reset() {
   BYTE picRes[6];
   int result;

   esyslog("alcd-plugin: ERROR: Performing PICRESET!");
   usleep(100000);
   // Clear read buffer
   result = read(fd,picRes,6);
   esyslog("alcd-plugin: Result Clear: %02x%02x%02x%02x%02x%02x (%d)",picRes[0],picRes[1],picRes[2],picRes[3],picRes[4],picRes[5], result);

   write(fd,PICRESET,2);
   usleep(10000);
   result = read(fd,picRes,2);
   esyslog("alcd-plugin: Result PICRESET: %02x%02x (%d)",picRes[0],picRes[1], result);

   if( resetScript != NULL ) {
      int rc;
      SerialPortClose();
      rc = system( resetScript );
      esyslog("alcd-plugin: Executing <%s> result: %x", resetScript, rc );
      SerialPortOpen();
   }

   AlcdSetup.LCDavailable = true;
   picError = false;
   actBrightness = -1;
   lRecStat = lInfoStat = lPowerLEDStat = -1;

   SetPowerButton(1);  // always enable PowerButton
   SetLCDBrightness(AlcdSetup.LCDBrightness);
   SetInfo(LED_OFF,LED_OFF,PWRLED_GREEN);

   SendCommand(LCD_CLEAR);
   actLine[0][0] = actLine[1][0] = '\x00';
   actPos[0] = actPos[1] = 0;
}

/////////////////////////////////////////////////////////
// Set Power Button

void activyLCD::SetPowerButton(bool status) { // switch powerbutton on
   switch( status ) {
      case 0:
         SendCommand(POWERBUTTON_SCANCODE_OFF);
         SendCommand(POWERBUTTON_OFF);
         break;

      case 1:
         SendCommand(POWERBUTTON_SCANCODE_ON);
         SendCommand(POWERBUTTON_ON);
         break;
   }
}

/////////////////////////////////////////////////////////
// Set LCD Brightness

void activyLCD::SetLCDBrightness(const int brightness) {
   if( brightness != actBrightness && brightness >= OFF && brightness <= BRIGHTEST ) {
      actBrightness = brightness;
      SendCommand(lcdBrightness[brightness]);
   }
}


/////////////////////////////////////////////////////////
// Send Heartbeat

void activyLCD::SetHeartbeat(unsigned int timeVal) {
   BYTE cmd[3];

   cmd[0] = (char)HEARTBEAT;
   if( timeVal > 0 && timeVal < 10 )
      timeVal = 10;

   cmd[1] = (BYTE)timeVal;
   cmd[2] = '\0';
   SendCommand(cmd);
}

/////////////////////////////////////////////////////////
// Set RecordLED

void activyLCD::SetInfo(int lRec, int lInfo, int lPwrLED) {
   const BYTE *cmd = NULL;

   if( lRec != -1 && lRecStat != lRec ) {
      lRecStat = lRec;
      if( lRecStat == LED_ON ) {
         if( AlcdSetup.LEDRecord == LEDREC_RED )
            cmd = MESSAGELED_ON;
         else
            cmd = BASICLED_ON;
      }
      else {
         if( AlcdSetup.LEDRecord == LEDREC_RED )
            cmd = MESSAGELED_OFF;
         else
            cmd = BASICLED_OFF;
      }

      SendCommand(cmd);
   }
   if( lInfo != -1 && lInfoStat != lInfo ) {
      lInfoStat = lInfo;
      if( lInfoStat == LED_ON ) {
         if( AlcdSetup.LEDRecord == LEDREC_RED )
            cmd = BASICLED_ON;
         else
            cmd = MESSAGELED_ON;
      }
      else {
         if( AlcdSetup.LEDRecord == LEDREC_RED )
            cmd = BASICLED_OFF;
         else
            cmd = MESSAGELED_OFF;
      }

      SendCommand(cmd);
   }

   if( lPwrLED != -1 && lPwrLED != lPowerLEDStat ) {
      if( lPowerLEDStat == PWRLED_BLINK )
         SendCommand(POWERLED_BLINK_OFF);

      lPowerLEDStat = lPwrLED;
      if( lPwrLED == PWRLED_RED )
         cmd = POWERLED_RED;
      else if( lPwrLED == PWRLED_BLINK )
         cmd = POWERLED_BLINK_ON;
      else
         cmd = POWERLED_GREEN;

      SendCommand(cmd);
   }
}

/////////////////////////////////////////////////////////
// Format String according to settings

void activyLCD::FormatString( BYTE *output, const char *input_s, int outLen, bool bSplit, bool bAlign ) {
   int bufLen = 0;
   BYTE outbuf[512];
   char input[2*strlen(input_s)];
   BYTE *outputPtr = outbuf;

   memset( outbuf, (int)' ', sizeof(outbuf) );

   Convert(input_s, input, 2*strlen(input_s));

#ifdef ALCD_DEBUG
   isyslog( "FormatString <%s>", input_s );
   isyslog( "FormatStr_Cv <%s>", input );
#endif

   BYTE buffer[256];
   char *in = (char *)input;
   if( bAlign )
      while( *in == ' ' )
         in++;

   memset( buffer, 0, sizeof(buffer) );
   BYTE *bufStart = buffer;
   BYTE *out = buffer;
   BYTE *lsp = out;
   BYTE *lbp = out;
   BYTE *endPtr = buffer + sizeof(buffer) - 2*LCD_COLUMNS;
   BYTE *nextSplitPos = out + LCD_COLUMNS;

   while( out < endPtr && *in != '\0' ) {
      if( ( AlcdSetup.Scrolling == SCROLL_BLOCK || bSplit == true ) && out >= nextSplitPos ) {
         BYTE *actPos;

         if( nextSplitPos - lbp < ( LCD_COLUMNS / 2 ) )
            actPos = lbp + 1;
         else if( nextSplitPos - lsp < ( LCD_COLUMNS / 2 ) )
            actPos = lsp + 1;
         else if( nextSplitPos - lbp < ( LCD_COLUMNS * 3 / 4 ) )
            actPos = lbp + 1;
         else if( nextSplitPos - lsp < ( LCD_COLUMNS * 3 / 4 ) )
            actPos = lsp + 1;
         else
            actPos = nextSplitPos;

         BYTE *strEnd = actPos - 1;
         // align the string
         int offset = 0;

         if( bAlign ) {
            // delete leading and trailing spaces
            while( *bufStart == ' ' && strEnd > bufStart )
               bufStart++;

            while( *strEnd == ' ' && strEnd > bufStart )
               strEnd--;

            bufLen = strEnd - bufStart + 1;

            if( AlcdSetup.Alignment == ALIGN_CENTER )
               offset = ( LCD_COLUMNS - bufLen ) / 2;
            else if( AlcdSetup.Alignment == ALIGN_RIGHT )
               offset = LCD_COLUMNS - bufLen;
         }
         else
            bufLen = LCD_COLUMNS;

         memcpy( outputPtr + offset, bufStart, bufLen );

         outputPtr += LCD_COLUMNS;

         if( bSplit == true ) {
            *outputPtr = '\0';
            outputPtr ++;
            bSplit = false;
         }

         lsp = lbp = actPos;
         bufStart = actPos;
         nextSplitPos = bufStart + LCD_COLUMNS;
         if( bAlign && *in == ' ' ) {
            while( *in == ' ' )
               in++;

            in--;            
         }

         if( *in == '\0' )
            break;
      }

      if( AlcdSetup.Umlauts == false ) {
         switch( *in ) {
            case '\x09':  // Tab
               *out = ' ';
               break;

            case '�':    // ae
               *out = 'a';
               out++;
               *out = 'e';
               break;

            case '�':    // oe
               *out = 'o';
               out++;
               *out = 'e';
               break;

            case '�':    // ue
               *out = 'u';
               out++;
               *out = 'e';
               break;

            case '�':    // ss
               *out = 's';
               out++;
               *out = 's';
               break;

            case '�':    // AE
               *out = 'A';
               out++;
               if( islower( *(in+1) ) )
                  *out = 'e';
               else
                  *out = 'E';
               break;

            case '�':    // OE
               *out = 'O';
               out++;
               if( islower( *(in+1) ) )
                  *out = 'e';
               else
                  *out = 'E';
               break;

            case '�':    // UE
               *out = 'U';
               out++;
               if( islower( *(in+1) ) )
                  *out = 'e';
               else
                  *out = 'E';
               break;

            default:
               if( *(BYTE*)in < 128 && *(BYTE*)in > 31 )
                  *out = *in;
               else
                  *out = '_';
               break;
         }
      }
      else {
         switch( *in ) {
            case '\x09':  // Tab
               *out = ' ';
               break;

            case '�':    // ae
               if( fwRev == ACTIVY_3XX )
                  *out = '\x84';
               else
                  *out = '\xE1';

               break;

            case '�':    // AE
               if( fwRev == ACTIVY_3XX )
                  *out = '\x8e';
               else
                  *out = '\xE1';
               break;

            case '�':    // oe
               if( fwRev == ACTIVY_3XX )
                  *out = '\x94';
               else
                  *out = '\xEF';
               break;

            case '�':    // OE
               if( fwRev == ACTIVY_3XX )
                  *out = '\x99';
               else
                  *out = '\xEF';
               break;

            case '�':    // ue
               if( fwRev == ACTIVY_3XX )
                  *out = '\x81';
               else
                  *out = '\xF5';
               break;

            case '�':    // UE
               if( fwRev == ACTIVY_3XX )
                  *out = '\x9a';
               else
                  *out = '\xF5';
               break;

            case '�':    // ss
               if( fwRev == ACTIVY_3XX )
                  *out = '\xe0';
               else
                  *out = '\xE2';
               break;

            default:
               if( *(BYTE*)in < 128 && *(BYTE*)in > 31 )
                  *out = *in;
               else
                  *out = '_';

               break;
         }
      }

#ifdef ALCD_DEBUG
      if( *(BYTE*)in > 127 || *(BYTE*)in < 32 )
         isyslog( "FormatString Conv<%c-%x><%c-%x>", *in, *(BYTE*)in, *out, *(BYTE*)out );
#endif

      if( *out == ' ' )
         lbp = out;
      else
         if( !isalnum( *out ) )
         lsp = out;

      out++;
      in++;
   }

   // center the string
   if( bAlign && (out - bufStart) < LCD_COLUMNS ) {
      out--;
      // delete leading and trailing spaces
      while( *bufStart == ' ' && out > bufStart )
         bufStart++;

      while( *out == ' ' && out > bufStart )
         out--;

      bufLen = out - bufStart + 1;

      if( AlcdSetup.Alignment == ALIGN_CENTER )
         outputPtr += ( LCD_COLUMNS - bufLen ) / 2;
      else if( AlcdSetup.Alignment == ALIGN_RIGHT )
         outputPtr += LCD_COLUMNS - bufLen;
   }
   else
      bufLen = out - bufStart;

   memcpy( outputPtr, bufStart, bufLen );

   outputPtr[ bufLen ] = '\0';
   if( bSplit )
      outputPtr[ bufLen + 1 ] = '\0';

   memcpy( output, outbuf, (outLen > (int)sizeof(outbuf)) ? sizeof(outbuf) : outLen );
   output[outLen - 1] = '\0';
#ifdef ALCD_DEBUG
   isyslog( "FormatString End<%s>", output );
#endif
}

void activyLCD::SetConv(void) {
   // Taken from VDR's vdr.c
   char *CodeSet = setlocale(LC_ALL, NULL);

   if( CodeSet && strcasestr(CodeSet, "UTF-8") != 0 )
      cd = iconv_open( "ISO-8859-1", "UTF-8" );
   else
      cd = (iconv_t)-1;

#ifdef ALCD_DEBUG
   isyslog( "CodeSet <%s>", CodeSet );
#endif
   cdResult = NULL;
   cdLength = 0;
}

void activyLCD::CloseConv(void) {
   free(cdResult);
   iconv_close(cd);
}

void activyLCD::Convert(const char *From, char *To, size_t ToLength) {
   if( cd != (iconv_t)-1 && From && *From ) {
      char *FromPtr = (char *)From;
      size_t FromLength = strlen(From);
      char *ToPtr = To;
      if( !ToPtr ) {
         cdLength = max(cdLength, FromLength * 2); // some reserve to avoid later reallocations
         cdResult = (char *)realloc(cdResult, cdLength);
         ToPtr = cdResult;
         ToLength = cdLength;
      }
      else if( !ToLength )
         return; // can't convert into a zero sized buffer
      ToLength--; // save space for terminating 0
      char *Converted = ToPtr;
      while( FromLength > 0 ) {
         if( iconv(cd, &FromPtr, &FromLength, &ToPtr, &ToLength) == size_t(-1) ) {
            if( errno == E2BIG || (errno == EILSEQ && ToLength < 1) ) {
               if( To )
                  break; // caller provided a fixed size buffer, but it was too small
               // The result buffer is too small, so increase it:
               size_t d = ToPtr - cdResult;
               size_t r = cdLength / 2;
               cdLength += r;
               Converted = cdResult = (char *)realloc(cdResult, cdLength);
               ToLength += r;
               ToPtr = cdResult + d;
            }
            if( errno == EILSEQ ) {
               // A character can't be converted, so mark it with '?' and proceed:
               FromPtr++;
               FromLength--;
               *ToPtr++ = '_';
               ToLength--;
            }
            else if( errno != E2BIG )
               return; // unknown error, return original string
         }
      }
      *ToPtr = 0;
   }
   else
      strncpy( To, From, ToLength );
}

#endif
