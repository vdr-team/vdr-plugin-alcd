///////////////////////////////////////////////////
// activy3xx LCD plugin for VDR
//
// Original written by:
//
// Meinrad Sauter <meinrad.sauter@gmx.de>
// andreas 'randy' weinberger <vdr@smue.org>
//
// rearranged, beautified and rewritten by:
//
// Markus Geisler <geisler@primusnetz.de>
//
// optimized by:
// Sascha Quint <sascha.quint@gmx.net>
//
// rearranged, beautified and rewritten by:
//
// Helmut Auer <helmut@helmutauer.de>
//
///////////////////////////////////////////////////

#ifndef __ALCD_SETUP_H
#define __ALCD_SETUP_H

#define SETUP_SLEEPTIME           "Sleeptime"
#define SETUP_SCROLLSPEED         "ScrollSpeed"
#define SETUP_SCROLLING           "Scrolling"
#define SETUP_UMLAUTS             "Umlauts"
#define SETUP_LEDRECORD           "LEDRecord"
#define SETUP_LCDBRIGHTNESS       "LCDBrightness"
#define SETUP_LCDAVAILABLE        "LCDavailable"
#define SETUP_LCDBRIGHTNESSONEXIT "LCDBrightnessOnExit"
#define SETUP_PROGRESSCHAR        "Progresschar"
#define SETUP_MSGTIME             "MessageTime"
#define SETUP_WATCHDOG            "Watchdog"
#define SETUP_ALIGNMENT           "Alignment"
#define SETUP_PROGRESSBAR         "Progressbar"
#define PROGRESS_SIGNS            "*#>|oO0X"

#define PB_OFF       0
#define PB_ON        1
#define PB_ON_TXT    2
#define PB_ONLY      3
#define PB_ONLY_TXT  4

#ifndef BYTE
typedef unsigned char BYTE;
#endif

class cAlcdSetup
{
public:
   int Alignment;
   int LCDavailable;
   int LCDBrightness;
   int LCDBrightnessOnExit;
   int LEDRecord;
   int MsgTime;
   unsigned char Progresschar;
   int Scrolling;
   int ScrollSpeed;
   int SleepTime;
   int Umlauts;
   int Watchdog;
   int Progressbar;

public:
   cAlcdSetup(void);
   virtual ~cAlcdSetup(void);
   cAlcdSetup & operator= (const cAlcdSetup & setup);
   void CopyFrom(const cAlcdSetup * source);
};

extern cAlcdSetup AlcdSetup;

#endif
