///////////////////////////////////////////////////
// activy3xx LCD plugin for VDR
//
// Original written by:
//
// Meinrad Sauter <meinrad.sauter@gmx.de>
// andreas 'randy' weinberger <vdr@smue.org>
//
// rearranged, beautified and rewritten by:
//
// Markus Geisler <geisler@primusnetz.de>
//
// optimized by:
// Sascha Quint <sascha.quint@gmx.net>
//
// rearranged, beautified and rewritten by:
// Helmut Auer <helmut@helmutauer.de>
//
///////////////////////////////////////////////////////////

#ifndef __ALCD_SETUP_CPP
#define __ALCD_SETUP_CPP

#include "setup.h"
#include "lcd.h"

cAlcdSetup AlcdSetup;

// Class cLcdSetup

cAlcdSetup::cAlcdSetup(void)
:  Alignment(ALIGN_CENTER),
   LCDavailable(1),
   LCDBrightness(BRIGHTEST),     // Brightness 0 == bright, 4 == dark...
   LCDBrightnessOnExit(DARKEST),
   LEDRecord(LEDREC_GREEN),      // Message LED
   MsgTime(5),
   Progresschar('#'),
   Scrolling(SCROLL_BLOCK),      // Kind of scrolling
   ScrollSpeed(3),
   SleepTime(10),
   Umlauts(0),              // Show Umlauts
   Watchdog(0),
   Progressbar(PB_ON)
{
}

cAlcdSetup::~cAlcdSetup(void)
{
}

cAlcdSetup & cAlcdSetup::operator=(const cAlcdSetup & setup) 
{
   CopyFrom( &setup );
   return *this;
}

void cAlcdSetup::CopyFrom(const cAlcdSetup * setup)
{
   Alignment = setup->Alignment;
   LCDavailable = setup->LCDavailable;
   LCDBrightness = setup->LCDBrightness;
   LCDBrightnessOnExit = setup->LCDBrightnessOnExit;
   LEDRecord = setup->LEDRecord;
   MsgTime = setup->MsgTime;
   Progresschar = setup->Progresschar;
   Scrolling = setup->Scrolling;
   ScrollSpeed = setup->ScrollSpeed;
   SleepTime = setup->SleepTime;
   Umlauts = setup->Umlauts;
   Watchdog = setup->Watchdog;
   Progressbar = setup->Progressbar;
}

#endif
