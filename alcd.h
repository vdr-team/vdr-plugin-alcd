///////////////////////////////////////////////////
// activy3xx LCD plugin for VDR
//
// Original written by:
//
// Meinrad Sauter <meinrad.sauter@gmx.de>
// andreas 'randy' weinberger <vdr@smue.org>
//
// rearranged, beautified and rewritten by:
//
// Markus Geisler <geisler@primusnetz.de>
//
// optimized by:
// Sascha Quint <sascha.quint@gmx.net>
//
// rearranged, beautified and rewritten by:
//
// Helmut Auer <helmut@helmutauer.de>
//
///////////////////////////////////////////////////

#ifndef __ALCD_H
#define __ALCD_H

#include "thread.h"

///////////////////////////////////////////////////
// Class cPluginAlcd
class cPluginAlcd : public cPlugin {
private:
   cLCD *aLcd;

public:
   cPluginAlcd(void);
   virtual ~cPluginAlcd();
   virtual const char *Version(void);
   virtual const char *Description(void);
   virtual const char *CommandLineHelp(void);
   virtual bool ProcessArgs(int argc, char *argv[]);
   virtual bool Start(void);
   virtual void Housekeeping(void);
   virtual const char *MainMenuEntry(void);
   virtual cOsdMenu *MainMenuAction(void);
   virtual cMenuSetupPage *SetupMenu(void);
   virtual bool SetupParse(const char *Name, const char *Value);
   virtual const char **SVDRPHelpPages(void);
   virtual cString SVDRPCommand(const char *Command, const char *Option, int &ReplyCode);
};

///////////////////////////////////////////////////
// Class cMenuSetupAlcd
class cMenuSetupAlcd : public cMenuSetupPage {
private:
   cAlcdSetup newAlcdSetup;

protected:
   virtual void Store(void);

public:
   cMenuSetupAlcd(void);
};

#endif
