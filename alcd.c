///////////////////////////////////////////////////
// activy3xx LCD plugin for VDR
//
// Original written by:
//
// Meinrad Sauter <meinrad.sauter@gmx.de>
// andreas 'randy' weinberger <vdr@smue.org>
//
// rearranged, beautified and rewritten by:
//
// Markus Geisler <geisler@primusnetz.de>
//
// optimized by:
// Sascha Quint <sascha.quint@gmx.net>
//
// rearranged, beautified and rewritten by:
// Helmut Auer <helmut@helmutauer.de>
//
///////////////////////////////////////////////////////////

#ifndef __ALCD_CPP
#define __ALCD_CPP

#include <getopt.h>
#include <stdlib.h>
#include <vdr/status.h>
#include <vdr/plugin.h>

#include "setup.h"
#include "alcd.h"
#include "thread.h"

// Statische Variablen
static const char *VERSION        = "1.5.2";
static const char *DESCRIPTION    = tr("Activy3xx LCD-Plugin");
static const char *MAINMENUENTRY  = NULL;

char *resetScript = NULL;

///////////////////////////////////////////////////////////
// Class cPluginAlcd Member Implementations

cPluginAlcd::cPluginAlcd(void) {
   // Initialize any member varaiables here.
   // DON'T DO ANYTHING ELSE THAT MAY HAVE SIDE EFFECTS, REQUIRE GLOBAL
   // VDR OBJECTS TO EXIST OR PRODUCE ANY OUTPUT!
   aLcd = NULL;
}

cPluginAlcd::~cPluginAlcd() {
   // Clean up after yourself!
   delete aLcd;
}

const char *cPluginAlcd::Version(void) {
   return VERSION;
}

const char *cPluginAlcd::Description(void) {
   return DESCRIPTION;
}

const char *cPluginAlcd::MainMenuEntry(void) {
   return MAINMENUENTRY;
}

const char *cPluginAlcd::CommandLineHelp(void) {
   // Return a string that describes all known command line options.
   static char *help_str=0;

   free( help_str );
   asprintf( &help_str, "  -r <file>,  --resetscript=<file>  script to execute after pic reset\n"
             "            (usually something like /etc/init.d/activy.sh)\n" );
   return help_str;
}

bool cPluginAlcd::ProcessArgs(int argc, char *argv[]) {
   // Implement command line argument processing here if applicable.
   static struct option long_options[] = {
      { "resetscript", required_argument, NULL, 'r'},
      { NULL}};

   int c, option_index = 0;

   while( ( c = getopt_long( argc, argv, "r:", long_options, &option_index ) ) != -1 ) {
      switch( c ) {
         case 'r':  resetScript = optarg; break;
         default:  return false;
      }
   }

   return true;
}

bool cPluginAlcd::Start(void) {
//     	isyslog("Calling cPluginAlcd::Start");
   // Start any background activities the plugin shall perform.
   aLcd = new cLCD();
   aLcd->Start();
   return true;
}

void cPluginAlcd::Housekeeping(void) {
   // Perform any cleanup or other regular tasks.
}

cOsdMenu *cPluginAlcd::MainMenuAction(void) {
   // Perform the action when selected from the main VDR menu.
   return NULL;
}

cMenuSetupPage *cPluginAlcd::SetupMenu(void) {
   // Return a setup menu in case the plugin supports one.
   return new cMenuSetupAlcd;
}

bool cPluginAlcd::SetupParse(const char *Name, const char *Value) {
   // Parse your own setup parameters and store their values.
   if( !strcasecmp(Name, SETUP_SCROLLSPEED) )
      AlcdSetup.ScrollSpeed = atoi(Value);
   else if( !strcasecmp(Name, SETUP_SLEEPTIME) )
      AlcdSetup.SleepTime = atoi(Value);
   else if( !strcasecmp(Name, SETUP_MSGTIME) )
      AlcdSetup.MsgTime = atoi(Value);
   else if( !strcasecmp(Name, SETUP_WATCHDOG) )
      AlcdSetup.Watchdog = atoi(Value);
   else if( !strcasecmp(Name, SETUP_LEDRECORD) )
      AlcdSetup.LEDRecord = atoi(Value);
   else if( !strcasecmp(Name, SETUP_UMLAUTS) )
      AlcdSetup.Umlauts = atoi(Value);
   else if( !strcasecmp(Name, SETUP_ALIGNMENT) )
      AlcdSetup.Alignment = atoi(Value);
   else if( !strcasecmp(Name, SETUP_SCROLLING) )
      AlcdSetup.Scrolling = atoi(Value);
   else if( !strcasecmp(Name, SETUP_LCDBRIGHTNESS) )
      AlcdSetup.LCDBrightness = atoi(Value);
   else if( !strcasecmp(Name, SETUP_LCDAVAILABLE) )
      AlcdSetup.LCDavailable = atoi(Value);
   else if( !strcasecmp(Name, SETUP_LCDBRIGHTNESSONEXIT) )
      AlcdSetup.LCDBrightnessOnExit = atoi(Value);
   else if( !strcasecmp(Name, SETUP_PROGRESSCHAR) )
      AlcdSetup.Progresschar = (char)atoi(Value);
   else if( !strcasecmp(Name, SETUP_PROGRESSBAR) )
      AlcdSetup.Progressbar = (char)atoi(Value);
   else
      return false;

   return true;
}

const char **cPluginAlcd::SVDRPHelpPages(void) {
   static const char *HelpPages[] = {
      "LOCK\n"
      "    stop vdr mode.",
      "UNLOCK\n"
      "    start vdr mode.",
      "SHOW <text>\n"
      "    display text.",
      "LED [OFF OFF|OFF ON|ON OFF|ON ON]\n"
      "    set LED's.",
      "PWRLED [BLINK|GREEN|RED]\n"
      "    set PowerLED.",
      "STAY [ON|OFF]\n"
      "    let Text stay after shutdown.",
      NULL
   };
   return HelpPages;
}

cString cPluginAlcd::SVDRPCommand(const char *Command, const char *Option, int &ReplyCode) {
   if( !strcasecmp(Command, "LOCK") ) {
      aLcd->Lock( true );
      return cString::sprintf("Display is locked");
   }
   else if( !strcasecmp(Command, "UNLOCK") ) {
      aLcd->Lock( false );
      return cString::sprintf("Display is unlocked");
   }
   else if( !strcasecmp(Command, "SHOW") ) {
      aLcd->SetText(Option);
      return cString::sprintf("String written to Display");
   }
   else if( !strcasecmp(Command, "LED") && Option != NULL ) {
      if( !strcasecmp(Option, "OFF OFF") ) {
         aLcd->SetLED(false,false);
      }
      else if( !strcasecmp(Option, "OFF ON") ) {
         aLcd->SetLED(false,true);
      }
      else if( !strcasecmp(Option, "ON OFF") ) {
         aLcd->SetLED(true,false);
      }
      else if( !strcasecmp(Option, "ON ON") ) {
         aLcd->SetLED(true,true);
      }
      else {
         return cString::sprintf("Invalid Option for LED");
      }
      return cString::sprintf("LED set");
   }
   else if( !strcasecmp(Command, "PWRLED") && Option != NULL ) {
      if( !strcasecmp(Option, "BLINK") ) {
         aLcd->SetPowerLED(PWRLED_BLINK);
      }
      else if( !strcasecmp(Option, "GREEN") ) {
         aLcd->SetPowerLED(PWRLED_GREEN);
      }
      else if( !strcasecmp(Option, "RED") ) {
         aLcd->SetPowerLED(PWRLED_RED);
      }
      else {
         return cString::sprintf("Invalid Option for PWRLED");
      }
      return cString::sprintf("PWRLED set");
   }
   else if( !strcasecmp(Command, "STAY") && Option != NULL ) {
      if( !strcasecmp(Option, "ON") ) {
         aLcd->SetTextPermanent(true);
      }
      else if( !strcasecmp(Option, "OFF") ) {
         aLcd->SetTextPermanent(false);
      }
      else {
         return cString::sprintf("Invalid Option for STAY");
      }
      return cString::sprintf("STAY Option set to %s", Option);
   }
   return NULL;
}

///////////////////////////////////////////////////////////
// Class cMenuSetupAlcd Member Implementations

cMenuSetupAlcd::cMenuSetupAlcd(void) {
   static const char *LEDRec[2];
   LEDRec[LEDREC_RED] = tr("upper LED blinking");
   LEDRec[LEDREC_GREEN] = tr("lower LED");

   static const char *Scroll[2];
   Scroll[SCROLL_BLOCK] = tr("By block");
   Scroll[SCROLL_CHAR] = tr("By character");

   static const char *OnOff[2];
   OnOff[0] = tr("off");
   OnOff[1] = tr("on");

   static const char *Aligntext[3];
   Aligntext[0] = tr("center");
   Aligntext[1] = tr("left");
   Aligntext[2] = tr("right");

   static const char *Progbar[5];
   Progbar[0] = tr("off");
   Progbar[1] = tr("on");
   Progbar[2] = tr("on + text");
   Progbar[3] = tr("only");
   Progbar[4] = tr("only + text");

   newAlcdSetup = AlcdSetup;

   Add(new cMenuEditStraItem( tr("Record LED"),  &newAlcdSetup.LEDRecord, 2, LEDRec));
   Add(new cMenuEditBoolItem( tr("Display available"), &newAlcdSetup.LCDavailable ));
   Add(new cMenuEditIntItem( tr("Sleeptime in deciseconds"), &newAlcdSetup.SleepTime, 1, 100));
   Add(new cMenuEditIntItem( tr("Message display (in seconds)"), &newAlcdSetup.MsgTime, 1, 9999));
   Add(new cMenuEditIntItem( tr("Scroll speed (factor of Sleeptime)"), &newAlcdSetup.ScrollSpeed, 0, 100));
   Add(new cMenuEditStraItem( tr("Scroll behaviour"),  &newAlcdSetup.Scrolling, 2, Scroll));
   Add(new cMenuEditStraItem( tr("Umlauts"),  &newAlcdSetup.Umlauts, 2, OnOff));
   Add(new cMenuEditStraItem( tr("Alignment"),  &newAlcdSetup.Alignment, 3, Aligntext));
   Add(new cMenuEditIntItem( tr("Display Brightness"), &newAlcdSetup.LCDBrightness, OFF, BRIGHTEST));
   Add(new cMenuEditIntItem( tr("Display Brightness on exit"), &newAlcdSetup.LCDBrightnessOnExit, OFF, BRIGHTEST));
   Add(new cMenuEditChrItem( tr("Progress Sign"), (char *)&newAlcdSetup.Progresschar, (const char *)PROGRESS_SIGNS));
   Add(new cMenuEditStraItem( tr("Progress Bar"),  &newAlcdSetup.Progressbar, 5, Progbar));
   Add(new cMenuEditIntItem( tr("Watchdog"), &newAlcdSetup.Watchdog, 0, 100 ));
}

void cMenuSetupAlcd::Store(void) {
   SetupStore(SETUP_SLEEPTIME,           AlcdSetup.SleepTime = newAlcdSetup.SleepTime);
   SetupStore(SETUP_SCROLLSPEED,         AlcdSetup.ScrollSpeed = newAlcdSetup.ScrollSpeed);
   SetupStore(SETUP_SCROLLING,           AlcdSetup.Scrolling = newAlcdSetup.Scrolling);
   SetupStore(SETUP_UMLAUTS,             AlcdSetup.Umlauts = newAlcdSetup.Umlauts);
   SetupStore(SETUP_LEDRECORD,           AlcdSetup.LEDRecord = newAlcdSetup.LEDRecord);
   SetupStore(SETUP_LCDBRIGHTNESS,       AlcdSetup.LCDBrightness = newAlcdSetup.LCDBrightness);
   SetupStore(SETUP_LCDAVAILABLE,        AlcdSetup.LCDavailable = newAlcdSetup.LCDavailable);
   SetupStore(SETUP_ALIGNMENT,           AlcdSetup.Alignment = newAlcdSetup.Alignment);
   SetupStore(SETUP_LCDBRIGHTNESSONEXIT, AlcdSetup.LCDBrightnessOnExit = newAlcdSetup.LCDBrightnessOnExit);
   SetupStore(SETUP_PROGRESSCHAR,        AlcdSetup.Progresschar = newAlcdSetup.Progresschar);
   SetupStore(SETUP_PROGRESSBAR,         AlcdSetup.Progressbar = newAlcdSetup.Progressbar);
   SetupStore(SETUP_MSGTIME,             AlcdSetup.MsgTime = newAlcdSetup.MsgTime);
   SetupStore(SETUP_WATCHDOG,            AlcdSetup.Watchdog = newAlcdSetup.Watchdog);
}


VDRPLUGINCREATOR(cPluginAlcd); // Don't touch this!

#endif
