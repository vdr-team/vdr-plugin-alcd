#define VERSION "0.1"
#define NAME "afp-tool"

#include <string>
#include <vector>

#include <getopt.h>
#include <stdarg.h>

#include "setup.h"
#include "lcd.h"

// sonstige Variablen
unsigned int ScrollSpeed = 3;
char *resetScript = NULL;
bool debug = false;

int SysLogLevel = 2;

#define MAXSYSLOGBUF 256

void syslog_with_tid(int priority, const char *format, ...) {
   char buffer[256];
   va_list args;
   va_start (args, format);
   vsnprintf (buffer,sizeof(buffer),format, args);
   if( debug )
      printf("%s\n", buffer);
   va_end(args);
}


void usage(void) {
   printf( "\n%s Version %s", NAME, VERSION );
   printf( "\n\nSyntax: %s [option]", NAME );
   printf( "\n   Available options are:" );
   printf( "\n     --align,         -a <0|1|2>  Align Text: 0=center,1=left,2=right\n" );
   printf( "\n     --text,          -t <string> Show text. Use | to split text into 2 lines\n" );
   printf( "\n     --brightness,    -b <0-16>   Set brightness. Use 0 for off\n" );
   printf( "\n     --heartbeat,     -h <n>      Set heartbeat to n*6 seconds, 0 = off\n" );
   printf( "\n     --stay,          -s <0|1>    Keep text after suspend\n" );
   printf( "\n     --powerled-red   -R <0|1>    Color of PowerLED (1=red)\n" );
   printf( "\n     --powerled-blink -B <0|1>    PowerLED blinking\n" );
   printf( "\n     --basic-led      -l <0|1>    (De)Activate basic LED\n" );
   printf( "\n     --message-led    -L <0|1>    (De)Activate message LED\n" );
   printf( "\n     --powerbutton    -x <0|1>    Activate Powerbutton\n" );
   printf( "\n     --reset,         -r          Send reset to frontpanel(not pc reset)\n" );
   printf( "\n     --magicreset,    -m          Send magic reset to frontpanel(not pc reset)\n" );
   printf( "\n     --verbose,       -v          Be verbose\n" );
   printf( "\n     --help,          -?          Show this help text\n\n" );

   exit(1);
}

int main(int argc, char **argv) {
   activyLCD * myLCD;
   bool parmErr = false;
   int heartBeat = -1;
   bool doMReset = false;
   bool doReset = false;
   bool doBrightness = false;
   bool doAlign = false;
   int stay = -1;
   int pLEDred = -1;
   int pLEDblink = -1;
   int bLED = -1;
   int mLED = -1;
   int pBut = -1;
   char * text = NULL;
   int valInt;
   int c;

   static struct option long_options[] = {
      { "heartbeat",      required_argument, NULL, 'h'},
      { "align",          required_argument, NULL, 'a'},
      { "text",           required_argument, NULL, 't'},
      { "brightness",     required_argument, NULL, 'b'},
      { "stay",           required_argument, NULL, 's'},
      { "powerled-red",   required_argument, NULL, 'R'},
      { "powerled-blink", required_argument, NULL, 'B'},
      { "basic-led",      required_argument, NULL, 'l'},
      { "message-led",    required_argument, NULL, 'L'},
      { "powerbutton",    required_argument, NULL, 'x'},
      { "reset",          no_argument, NULL, 'r'},
      { "magicreset",     no_argument, NULL, 'm'},
      { "verbose",        no_argument, NULL, 'v'},
      { "help",           no_argument, NULL, '?'},
      { 0,0,0,0}
   };

   if( argc < 2 ) {
      parmErr = true;
   }
   while( !parmErr && (c = getopt_long(argc, argv, "vmr?h:a:t:b:s:R:B:l:L:x:", long_options, NULL)) != -1 ) {
      if( optarg )
         valInt = atoi(optarg);
      else
         valInt = -9999;

      switch( c ) {
         case 'h':
            if( valInt >= 0 )
               heartBeat = valInt;
            else
               parmErr = true;
            break;
         case 'a':
            if( valInt == ALIGN_CENTER || valInt == ALIGN_LEFT || valInt == ALIGN_RIGHT ) {
               AlcdSetup.Alignment = valInt;
               doAlign = true;
            }
            else
               parmErr = true;
            break;
         case 't':
            text = strdup( optarg );
            break;
         case 'b':
            if( valInt >= OFF && valInt <= BRIGHTEST ) {
               AlcdSetup.LCDBrightness = valInt;
               doBrightness = true;
            }
            else
               parmErr = true;
            break;
         case 's':
            if( valInt >= 0 && valInt <= 1 )
               stay = valInt;
            else
               parmErr = true;
            break;
         case 'R':
            if( valInt >= 0 && valInt <= 1 )
               pLEDred = valInt;
            else
               parmErr = true;
            break;
         case 'B':
            if( valInt >= 0 && valInt <= 1 )
               pLEDblink = valInt;
            else
               parmErr = true;
            break;
         case 'l':
            if( valInt >= 0 && valInt <= 1 )
               bLED = valInt;
            else
               parmErr = true;
            break;
         case 'L':
            if( valInt >= 0 && valInt <= 1 )
               mLED = valInt;
            else
               parmErr = true;
            break;
         case 'x':
            if( valInt >= 0 && valInt <= 1 )
               pBut = valInt;
            else
               parmErr = true;
            break;
         case 'r':
            doReset = true;
            break;
         case 'm':
            doMReset = true;
            break;
         case 'v':
            debug = true;
            break;
         case '?':
            parmErr = true;
            break;
         default:
            parmErr = true;
            break;
      }
   }
   if( parmErr )
      usage();

   myLCD = new activyLCD(false);

   if( debug ) {
      myLCD->SendCommand(HWREV);
      myLCD->SendCommand(FWREV);
   }

   if( doMReset ) {
      printf( "Sending Magic Reset\n" );
      myLCD->SendCommand(MAGIC_RESET);
   }
   if( doReset ) {
      printf( "Sending Pic Reset\n" );
      myLCD->SendCommand(PICRESET);
   }
   if( doBrightness ) {
      printf( "Sending brightness %d\n", AlcdSetup.LCDBrightness );
      myLCD->SetLCDBrightness(AlcdSetup.LCDBrightness);
   }
   if( heartBeat >= 0 ) {
      printf( "Setting heartbeat %d\n", heartBeat );
      myLCD->SetHeartbeat(heartBeat);
   }
   if( stay >= 0 ) {
      printf( "Setting permanent %d\n", stay );
      myLCD->SetTextPermanent(stay);
   }
   if( pLEDred >= 0 ) {
      printf( "Setting powerled color %d\n", pLEDred );
      myLCD->SendCommand( pLEDred ? POWERLED_RED : POWERLED_GREEN );
   }
   if( pLEDblink >= 0 ) {
      printf( "Setting powerled blink %d\n", pLEDblink );
      myLCD->SendCommand( pLEDblink ? POWERLED_BLINK_ON : POWERLED_BLINK_OFF );
   }
   if( bLED >= 0 ) {
      printf( "Setting basic led %d\n", bLED );
      myLCD->SendCommand( bLED ? BASICLED_ON : BASICLED_OFF );
   }
   if( mLED >= 0 ) {
      printf( "Setting message led %d\n", bLED );
      myLCD->SendCommand( mLED ? MESSAGELED_ON : MESSAGELED_OFF );
   }
   if( pBut >= 0 ) {
      printf( "Setting powerbutton %d\n", pBut );
      myLCD->SetPowerButton( pBut );
   }
   if( text != NULL ) {
      BYTE szLine1[LCD_COLUMNS+1];
      BYTE szLine2[LCD_COLUMNS+1];
      BYTE * szLine[LCD_ROWS] = { szLine1, szLine2};

      if( strlen(text) > 0 ) {
         char *p = strchr( text, '|' );

         if( p != NULL ) {
            *p = '\0';
            p++;
         }
         myLCD->FormatString( szLine1, text, LCD_COLUMNS, false, doAlign );
         myLCD->FormatString( szLine2, p, LCD_COLUMNS, false, doAlign );
      }
      else {
         memset( szLine1, ' ', sizeof(szLine1) );
         memset( szLine2, ' ', sizeof(szLine2) );
         szLine1[sizeof(szLine1)-1] = '\0';
         szLine2[sizeof(szLine2)-1] = '\0';
      }

      printf( "Sending text <%s><%s>\n", szLine1, szLine2 );
      myLCD->SendText( (const BYTE **)szLine );
   }

   delete myLCD;
   exit(0);
} 

